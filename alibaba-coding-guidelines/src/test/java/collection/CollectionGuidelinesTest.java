package collection;

import cn.hutool.core.lang.Pair;
import org.junit.Test;
import org.testng.Assert;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 */
public class CollectionGuidelinesTest {

    @Test
    public void test_01() {
        Map<Item,String> hashmap = new HashMap<>();
        hashmap.put(new Item("f1","f2"),"it1");
        hashmap.put(new Item("f1","f2"),"it1");
        /**
         * hashMap使用put方法之前会调用hash方法取获取key的hashCode值，
         * 然后根据hash方法返回的boolean值来决定put操作
         */
        Assert.assertEquals(hashmap.size(), 1);

    }
    @Test
    public void test_02() {
        Map<String, Object> map = new HashMap<>();
        if (map.isEmpty()) {
            System.out.println("no element in this map.");
        }
    }

    @Test
    public void test_03_good() {
        List<Pair<String, Double>> pairArrayList = new ArrayList<>(3);
        pairArrayList.add(new Pair<>("version", 12.10));
        pairArrayList.add(new Pair<>("version", 12.19));
        pairArrayList.add(new Pair<>("version", 6.28));
        // 生成的 map 集合中只有一个键值对：{version=6.28}
        Map<String, Double> map = pairArrayList.stream()
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue, (v1, v2) -> v2));
        Assert.assertEquals(map.size(), 1);
    }

    @Test(expected = IllegalStateException.class)
    public void test_03_bad() {
        String[] departments = new String[]{"RDC", "RDC", "KKB"};
        Map<Integer, String> map = Arrays.stream(departments)
                .collect(Collectors.toMap(String::hashCode, str -> str));
    }


    @Test(expected = NullPointerException.class)
    public void test_04_bad() {
        List<Pair<String, Double>> pairArrayList = new ArrayList<>(2);
        pairArrayList.add(new Pair<>("version1", 8.3));
        pairArrayList.add(new Pair<>("version2", null));
        // 抛出 NullPointerException 异常
        Map<String, Double> map = pairArrayList.stream()
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue, (v1, v2) -> v2));
    }

    @Test
    public void test_05() {
        List<Integer> scores = new ArrayList<>();
        scores.add(50);
        scores.add(51);
        scores.add(52);
        scores.add(53);
        scores.add(54);
        scores.add(55);
        scores.add(56);

        List<Integer> subScores = scores.subList(1,3);
        Assert.assertTrue(subScores.size() == 2);
        subScores.set(0,100);
        Assert.assertTrue(scores.get(1) == 100);
    }

    @Test(expected = ConcurrentModificationException.class)
    public void test_06() {
        Map<String, String> studentMap = new HashMap<>();
        studentMap.put("1001", "zhangsan");
        studentMap.put("1002", "lisi");
        studentMap.put("1003", "wangwu");
        studentMap.put("1004", "zhaoliu");

        for (Map.Entry<String, String> entry: studentMap.entrySet()) {
            System.out.println(entry);
            studentMap.put("1008", "xxxx");
        }
    }
    @Test(expected = UnsupportedOperationException.class)
    public void test_11_0() {
        String[] str = new String[]{ "yang", "guan", "bao" };
        List list = Arrays.asList(str);
        list.add("yangguanbao");
    }

    @Test
    public void test_11_1() {
        String[] str = new String[]{ "yang", "guan", "bao" };
        List list = Arrays.asList(str);
        Assert.assertEquals(list.get(0), "yang");

        str[0] = "change";
        Assert.assertEquals(list.get(0), "change");
    }

    @Test(expected = ClassCastException.class)
    public void test_13_bad() {
        List<String> generics = null;
        List notGenerics = new ArrayList(10);
        notGenerics.add(new Object());
        notGenerics.add(new Integer(1));
        generics = notGenerics;
        // 此处抛出 ClassCastException 异常
        String string = generics.get(0);
    }

   static class Item {
        private  String field1;
        private  String field2;

       public Item(String field1, String field2) {
           this.field1 = field1;
           this.field2 = field2;
       }

       public String getField1() {
           return field1;
       }

       public void setField1(String field1) {
           this.field1 = field1;
       }

       public String getField2() {
           return field2;
       }

       public void setField2(String field2) {
           this.field2 = field2;
       }

       /**
        * 按照默认的重载equals和hashcode
        * @param o
        * @return
        */
       @Override
       public boolean equals(Object o) {
           if (this == o) return true;
           if (o == null || getClass() != o.getClass()) return false;

           Item item1 = (Item) o;
           /**
            * 还比较了里面属性的值是否一致
            */
           if (field1 != null ? !field1.equals(item1.field1) : item1.field1 != null) return false;
           return field2 != null ? field2.equals(item1.field2) : item1.field2 == null;
       }

       @Override
       public int hashCode() {
           int result = field1 != null ? field1.hashCode() : 0;
           result = 31 * result + (field2 != null ? field2.hashCode() : 0);
           return result;
       }

   }
}

