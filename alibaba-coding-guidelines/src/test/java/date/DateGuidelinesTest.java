package date;

import cn.hutool.core.date.DateUtil;
import org.junit.Test;
import org.testng.Assert;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

/**
 * java.lang.Object ....|__java.util.Date ..........|__java.sql.Date/java.sql.Timestamp /java.sql.Time
 * 【父类】java.util.Date日期格式为：年月日时分秒 【子类】java.sql.Date日期格式为：年月日[只存储日期数据不存储时间数据] 【子类】java.sql.Time日期格式为：时分秒
 * 【子类】java.sql.Timestamp日期格式为：年月日时分秒纳秒（毫微秒）
 */
public class DateGuidelinesTest {

    @Test
    public void test_01() {
        Date date = new Date(1514713596000L);

        SimpleDateFormat sdfGood = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Assert.assertEquals(sdfGood.format(date), "2017-12-31 17:46:36");

        SimpleDateFormat sdfBad = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Assert.assertEquals(sdfBad.format(date), "2018-12-31 17:46:36");
    }

    @Test
    public void test_02() {
        Date date = new Date(1514713596000L);

        SimpleDateFormat sdf0 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Assert.assertEquals(sdf0.format(date), "2017-12-31 17:46:36");

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Assert.assertEquals(sdf1.format(date), "2017-12-31 05:46:36");
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_04_1() {
        java.sql.Date date = new java.sql.Date(1514713596000L);
        date.getHours();
    }

    @Test(expected = IllegalArgumentException.class)
    public void test_04_2() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        java.sql.Time sqlTime = java.sql.Time.valueOf("13:44:53");
        Assert.assertEquals(sdf.format(sqlTime), "13:44:53");

        java.sql.Time time1 = new java.sql.Time(1514713596000L);
        time1.getYear();
    }

    @Test
    public void test_04_3() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
        java.sql.Timestamp timestamp = java.sql.Timestamp.valueOf("2010-08-20 14:06:27.186");
        Assert.assertEquals(sdf.format(timestamp), "2010-08-20 14:06:27.186");
    }

    @Test
    public void test_04_4() {
        java.sql.Time sqlTime = java.sql.Time.valueOf("13:44:53");

        Date date = new Date();

        date.after(sqlTime);
    }

    @Test
    public void test_05() {
        // 获取今年的天数
        int daysOfThisYear = LocalDate.now().lengthOfYear();
        // 获取指定某年的天数
        int daysOf2011Year = LocalDate.of(2011, 1, 1).lengthOfYear();
        Assert.assertEquals(daysOfThisYear, 365);
        Assert.assertEquals(daysOf2011Year, 365);
    }



    @Test
    public void test_hutool() {
        Date date = new Date(1514713596000L);
        String formatDateTime = DateUtil.formatDateTime(date);
        Assert.assertEquals(formatDateTime, "2017-12-31 17:46:36");
    }

}
