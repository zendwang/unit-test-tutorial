import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import org.junit.Test;

import com.google.common.base.Optional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class CacheClearTest {

	static Cache<Integer, String> cache = CacheBuilder.newBuilder()
			.expireAfterWrite(5, TimeUnit.SECONDS)
			.build(new CacheLoader<Integer, String>() {
				@Override
				public String load(Integer integer) throws Exception {
					return null;
				}
			});

	/**
	 *
	 * cache 清理机制：写操作时顺带做少量维护工作或者偶尔在读操作时
	 *
	 * 原因：如果要自动地持续清理缓存，就必须有一个线程，这个线程会和用户操作竞争共享锁。
	 * 此外，某些环境下线程创建可能受限制，这样CacheBuilder就不可用了
	 * @throws InterruptedException
	 */
	@Test
	public void testCacheClear() throws InterruptedException {
		new Thread(() -> {
			while (true) {
				SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
				System.out.println(sdf.format(new Date()) + " size: " + cache.size());
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {

				}
			}
		}).start();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		cache.put(1, "a");
		System.out.println("写入 key:1 ,value:" + cache.getIfPresent(1));
		Thread.sleep(10000);
		cache.put(2, "b");
		System.out.println("写入 key:2 ,value:" + cache.getIfPresent(2));
		Thread.sleep(10000);
		System.out.println(sdf.format(new Date())
				+ " sleep 10s , key:1 ,value:" + cache.getIfPresent(1));
		System.out.println(sdf.format(new Date())
				+ " sleep 10s, key:2 ,value:" + cache.getIfPresent(2));

		//cache.cleanUp();

	}
}
