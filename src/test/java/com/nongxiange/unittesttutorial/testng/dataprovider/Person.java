package com.nongxiange.unittesttutorial.testng.dataprovider;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

public class Person {
    String dataFile;
    
    @Parameters({ "file" })  //parameter可以添加在类的构造方法上
    public Person(String dataFile) {
        super();
        this.dataFile = dataFile;
    }
    
    @Test(dataProvider = "provider") //被测试方法
    public void getName(String name, String age) {
        System.out.println(name + "_" + age);
    }
    
    @DataProvider(name = "provider")
    public Iterator<Object[]>  provider() throws IOException {
        return new TxtIterator(new File(dataFile));
    }
}
