package com.nongxiange.unittesttutorial.testng.dataprovider;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class ParamTestWithDataProvider2 {
    @Test(dataProvider = "provider")
    public void getFirst(String name, int age) {
        System.out.println("第一组"+name);
    }
    
    @Test(dataProvider = "provider")
    public void getSecond(String name, int age) {
        System.out.println("第二组 " + name);
    }
    
    @DataProvider(name = "provider")
    public Object[][] provider(Method method) {
        Object[][] objects;
        if (method.getName().equals("getFirst")) {              //如果调用该DataProvider的函数是getFirst，那么就返回这个数组
            objects = new Object[][] { { "cq1", 20 }, { "cq2", 22 } };
        } else if (method.getName().equals("getSecond")) {//如果调用该DataProvider的函数是getSecond，那么就返回这个数组
            objects = new Object[][] { { "cq3", 20 }, { "cq4", 22 } };
        } else {                                                                    //如果调用该DataProvider的函数不是getFirst也不是getSecond，那么就返回这个数组
            objects = new Object[][] { {"cq5",33}, {"cq6",34} };
        }
        
        return objects;
    }
}


