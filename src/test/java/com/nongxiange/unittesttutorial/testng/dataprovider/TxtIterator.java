package com.nongxiange.unittesttutorial.testng.dataprovider;

import java.io.*;
import java.util.Iterator;

public class TxtIterator implements Iterator<Object[]> {
    File txtFile;           //数据文件
    BufferedReader bs;
    String currentLine;
    
    public TxtIterator(File txtFile) throws IOException {
        super();
        this.txtFile = txtFile;
        try {
            bs = new BufferedReader(new FileReader(txtFile));
        } catch (FileNotFoundException e) {
            System.out.println("文件找不到");
            e.printStackTrace();
        }
        currentLine = bs.readLine();
    }
    
    @Override
    public boolean hasNext() {
        if (currentLine != null) {
            return true;
        } else {
            return false;
        }
    }
    
    @Override
    public Object[] next() {
        String returnLine = currentLine;
        try {
            currentLine = bs.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if ( returnLine.endsWith(",")) {
            returnLine = returnLine + " ";
        }
        Object[] data = returnLine.split(",");

        return data;
    }
    
}

