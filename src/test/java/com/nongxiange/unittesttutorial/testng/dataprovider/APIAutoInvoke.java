package com.nongxiange.unittesttutorial.testng.dataprovider;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.alibaba.fastjson.JSON;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import org.testng.annotations.*;

@Slf4j
public class APIAutoInvoke {

    String dataFile;

    String url;



    ThreadLocalRandom threadLocalRandom;

    private AtomicLong i = new AtomicLong(0);
//    OkHttpClient okHttpClient;

    ExecutorService executor = Executors.newFixedThreadPool(20);
    @BeforeClass
    private void setUp() {
//        OkHttpClient okHttpClient = new OkHttpClient.Builder()
//                //设置连接超时
//                .connectTimeout(10, TimeUnit.SECONDS)
//                //设置读超时
//                .readTimeout(30, TimeUnit.SECONDS)
//                //设置写超时
//                .writeTimeout(10, TimeUnit.SECONDS)
//                //是否自动重连
//                .retryOnConnectionFailure(true)
//                .connectionPool(new ConnectionPool(10, 10, TimeUnit.MINUTES))
//                .build();

    }
    @Parameters({ "file" })  //parameter可以添加在类的构造方法上
    public APIAutoInvoke(String dataFile) {
        super();
        this.dataFile = dataFile;
        this.threadLocalRandom = RandomUtil.getRandom();
        //this.url = "http://127.0.0.1:8081/dmp-api/cs/batch/import/Assign";
        this.url = "https://api.hongkang-life.com/dmp-api/cs/batch/import/Assign";
    }


    @Test(dataProvider = "provider") //被测试方法
    public void getName(String customerNo,String name, String idType,String idNo,String dataFrom, String mobile)  {
        if(!StrUtil.isAllNotBlank(customerNo, name,  idType, idNo, dataFrom,  mobile)) {
            log.info("i={} thread-name:{} parameters: customerNo={}, name={},  idType={}, idNo={}, dataFrom={},  mobile={}", i.incrementAndGet(), Thread.currentThread().getName(), customerNo, name,  idType, idNo, dataFrom,  mobile);
            return;
        }
        executor.submit(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                HashMap<String, Object> paramMap = new HashMap<>();
                paramMap.put("customerNo", Long.valueOf(customerNo));
                paramMap.put("name", name);
                paramMap.put("idType", idType);
                paramMap.put("idNo", idNo);
                paramMap.put("dataFrom", Integer.valueOf(dataFrom));
                paramMap.put("mobile", mobile);
                //log.info("i={} thread-name:{} parameters:{} ", Thread.currentThread().getName(), JSON.toJSONString(paramMap));
                HttpRequest request = HttpRequest.post(url).contentType("application/json");
                HttpResponse response = request.body(JSON.toJSONString(paramMap)).execute();
                //long sleepMillis = threadLocalRandom.nextLong(0L, 30L);
                log.info("i={} customerNo={} result:{}\n ",  i.incrementAndGet(), Long.valueOf(customerNo), response.body());
                //Thread.sleep( sleepMillis);
            }
        });
    }
    
    @DataProvider(name = "provider")
    public Iterator<Object[]>  provider() throws IOException {
        return new TxtIterator(new File(dataFile));
    }

    @AfterClass
    private void down() throws InterruptedException {

        executor.awaitTermination(20, TimeUnit.HOURS);

    }
}
