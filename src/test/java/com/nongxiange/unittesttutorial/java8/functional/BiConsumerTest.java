package com.nongxiange.unittesttutorial.java8.functional;

import java.util.function.BiConsumer;

/**
 * 消费型接口 返回类型 void
 * 接收两个泛型参数，
 * 调用accept方法 才会执行
 */
public class BiConsumerTest {
    
    public static void main(String[] args) {
        
        BiConsumer<Integer,String> biconsumer = (x, y) -> {
            int a = x + 2;
            System.out.println(a);
            System.out.println("----"+y);
        };
        biconsumer.accept(10,"我爱你");
        
        System.out.println("-----执行andThen()方法--------");
        BiConsumer<Integer,String> afterBiconsumer = (x,y) -> {
            int a = x + 8;
            System.out.println(a);
            System.out.println("----"+y);// ----哇呀呀
        };
        biconsumer.andThen(afterBiconsumer).accept(10,"哇呀呀");
    }
}
