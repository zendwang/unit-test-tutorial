package com.nongxiange.unittesttutorial.java8.functional;

import org.junit.Test;

import java.util.function.Consumer;

/**
 * 消费型接口 返回类型 void
 * 接收一个个泛型参数，
 * 调用accept方法 才会执行
 */
public class ConsumerTest {
    @Test
    public void test() {
        Consumer f = System.out::println;
        Consumer f2 = n -> System.out.println(n + "-F2");
    
        //执行完F后再执行F2的Accept方法
        f.andThen(f2).accept("test");

        System.out.println("-----------------------------");

        //连续执行F的Accept方法
        f.andThen(f).andThen(f).andThen(f).accept("test1");
    }
}
