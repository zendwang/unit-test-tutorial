package com.nongxiange.unittesttutorial.java8.functional;

import org.junit.Assert;
import org.junit.Test;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * 函数型接口 参数类型T, U 返回类型 R
 * 接收一个个泛型参数，
 * R apply(T t, U u)
 */
public class BiFunctionTest {

    @Test
    public void test() {
        BiFunction<Integer, Integer, Integer> action = (x, y) -> {
            System.out.println(x + y);
            return x + y;
        };
        Function<Integer, Integer> action2 = (x) -> {
            System.out.println("Function..." + x);
            return x;
        };
        BiFunction<Integer, Integer, Integer> integerIntegerBiConsumer = action.andThen(action2);
    
        Integer result = integerIntegerBiConsumer.apply(1, 1);
        Assert.assertTrue(2 == result);
    }
}
