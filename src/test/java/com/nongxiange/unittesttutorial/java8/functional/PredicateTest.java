package com.nongxiange.unittesttutorial.java8.functional;


import org.junit.Assert;

import java.util.function.Predicate;

/**
 * 断定型接口 参数类型T 返回类型 Boolean
 * 接收一个个泛型参数，
 * boolean test(T t)
 */
public class PredicateTest {
    
    public static void main(String[] args) {
        Predicate<String> p = o -> o.equals("test");
        Predicate<String> g = o -> o.startsWith("t");
    
        /**
         * negate: 用于对原来的Predicate做取反处理；
         * 如当调用p.test("test")为True时，调用p.negate().test("test")就会是False；
         */
        Assert.assertFalse(p.negate().test("test"));
    
        /**
         * and: 针对同一输入值，多个Predicate均返回True时返回True，否则返回False；
         */
        Assert.assertTrue(p.and(g).test("test"));
    
        /**
         * or: 针对同一输入值，多个Predicate只要有一个返回True则返回True，否则返回False
         */
        Assert.assertTrue(p.or(g).test("ta"));
    }
}
