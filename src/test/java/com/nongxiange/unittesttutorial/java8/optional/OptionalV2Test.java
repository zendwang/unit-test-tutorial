package com.nongxiange.unittesttutorial.java8.optional;

import org.junit.Assert;
import org.junit.Test;

import java.util.Optional;

/**
 * 参考
 * https://www.cnblogs.com/zhangboyu/p/7580262.html
 */
public class OptionalV2Test {
    
    @Test
    public void whenMap_thenOk() {
        User user = new User("jim","anna@gmail.com");
        String email = Optional.ofNullable(user)
                .map(u -> u.getEmail()).orElse("default@gmail.com");
    
        Assert.assertEquals(email, user.getEmail());
    }
    
    @Test
    public void whenFlatMap_thenOk() {
        User user = new User("jim","anna@gmail.com");
        user.setPosition("Developer");
        String position = Optional.ofNullable(user)
                .flatMap(u -> u.getPosition()).orElse("default");
        
        Assert.assertEquals(position, user.getPosition().get());
    }
    
    @Test
    public void whenFilter_thenOk() {
        User user = new User( "jim","anna@gmail.com");
        Optional<User> result = Optional.ofNullable(user)
                .filter(u -> u.getEmail() != null && u.getEmail().contains("@"));
    
        Assert.assertTrue(result.isPresent());
    }
    
    @Test
    public void givenEmptyValue_whenCompare_thenOk() {
        User user = null;
        System.out.println("Using orElse");
        System.out.println();
        User result = Optional.ofNullable(user).orElse(createNewUser());
        System.out.println("Using orElseGet");
        User result2 = Optional.ofNullable(user).orElseGet(() -> createNewUser());
    }
    
    /**
     * 两个 Optional  对象都包含非空值，两个方法都会返回对应的非空值。
     * 不过，orElse() 方法仍然创建了 User 对象。与之相反，orElseGet() 方法不创建 User 对象。
     * 在执行较密集的调用时，比如调用 Web 服务或数据查询，这个差异会对性能产生重大影响。
     */
    @Test
    public void givenPresentValue_whenCompare_thenOk() {
//        User user = new User("jim","extra@gmail.com");
        User user = null;
        System.out.println("Using orElse");
        User result = Optional.ofNullable(user).orElse(createNewUser());
        System.out.println("Using orElseGet");
        User result2 = Optional.ofNullable(user).orElseGet(() -> createNewUser());

        System.out.println("end");
    }
    
    private User createNewUser() {
        System.out.println("Creating New User");
        return new User("jim","extra@gmail.com");
    }
}
