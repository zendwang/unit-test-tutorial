package com.nongxiange.unittesttutorial.java8.optional;

import java.util.Optional;

public class User {
    private String name;
    private String email;
    private String position;
    
    public User(final String name, final String email) {
        this.name = name;
        this.email = email;
    }
    
    public String getName() {
        return name;
    }
    
    public String getEmail() {
        return email;
    }
    
    public Optional<String> getPosition() {
        return Optional.ofNullable(position);
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    public void setEmail(final String email) {
        this.email = email;
    }
    
    public void setPosition(final String position) {
        this.position = position;
    }
}
