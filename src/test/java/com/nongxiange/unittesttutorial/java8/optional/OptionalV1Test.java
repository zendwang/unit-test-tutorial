package com.nongxiange.unittesttutorial.java8.optional;

import org.junit.Test;

import java.util.Optional;
import java.util.Random;

public class OptionalV1Test {
    /**
     * 判断结果不为空后使用
     * 存在就输出
     */
    @Test
    public void testIfPresent() {
        Optional<String> s = Optional.ofNullable(randomStr());
        s.ifPresent(System.out::println);
    }
    


    /**
     * 变量为空时提供默认值
     */
    @Test
    public void testOrElse() {
        Optional<String> s = Optional.ofNullable(randomStr());
        System.out.println(s.orElse("test"));
    }
    
    //变量为空时抛出异常，否则使用
    @Test
    public void testOrElseThrow() {
        Optional<String> o = Optional.ofNullable(randomStr());
        try {
            System.out.println(o.orElseThrow(()->new Exception("test")));
        }catch (Exception ex) {
            System.out.println(ex);
        }
        
    }
    
    
    public String randomStr() {
        Random random = new Random();
        if (5 < random.nextInt(20)) {
            return "xxxx";
        }
        return null;
    }
    
}
