package com.nongxiange.unittesttutorial.java8.time;


import org.junit.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class LocalDataTest {
    @Test
    public void date2LocalDate() {
        Date date = new Date();
        //获得本地默认时区
        ZoneId defaultZoneId = ZoneId.systemDefault();
        LocalDate localDate = date.toInstant().atZone(defaultZoneId).toLocalDate();

        System.out.println(localDate.toString());
        System.out.println(localDate.getDayOfMonth());
    }

    @Test
    public void localDate2Date() {
        LocalDate localDate = Instant.now().atZone(ZoneId.systemDefault()).toLocalDate();
        localDate = localDate.minusDays(localDate.getDayOfMonth() - 1);
        ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.systemDefault());
        System.out.println(Date.from(zonedDateTime.toInstant()));
    }
}
