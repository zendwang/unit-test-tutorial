package com.nongxiange.unittesttutorial.java8.time;



import org.junit.Assert;
import org.junit.Test;

import java.time.*;
import java.time.format.DateTimeFormatter;


/**
 *
 ┌─────────────┐
 │LocalDateTime│────┐
 └─────────────┘    │    ┌─────────────┐
 ├───>│ZonedDateTime│
 ┌─────────────┐    │    └─────────────┘
 │   ZoneId    │────┘           ▲
 └─────────────┘      ┌─────────┴─────────┐
 │                   │
 ▼                  ▼
 ┌─────────────┐     ┌─────────────┐
 │   Instant   │<───>│    long     │
 └─────────────┘     └─────────────┘
 */
public class InstantTest {
    @Test
    public void instantNow() {
        Instant now = Instant.now();
        System.out.println(now.getEpochSecond()); // 秒
        System.out.println(now.toEpochMilli()); // 毫秒
    }
    @Test
    public void instant2ZonedDateTime() {
        Instant instant = Instant.ofEpochMilli(1568568760316l);//毫秒
        ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());//时间戳，给它关联上指定的ZoneId，就得到了ZonedDateTime
        Assert.assertEquals("20190916+0800", zdt.format(DateTimeFormatter.BASIC_ISO_DATE));
    }

    @Test
    public void instant2LocalDateTime() {
        Instant instant = Instant.ofEpochMilli(1568568760316l);//毫秒
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.of("Asia/Shanghai"));

        Assert.assertEquals("20190916", localDateTime.format(DateTimeFormatter.BASIC_ISO_DATE));
        Assert.assertEquals("2019-09-16", localDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
    }
    @Test
    public void instantWithPeriod() {
        Instant instant = Instant.ofEpochMilli(1568568760316l);//毫秒
        instant = instant.plus(Period.ofDays(1));
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.of("Asia/Shanghai"));

        Assert.assertEquals("20190917", localDateTime.format(DateTimeFormatter.BASIC_ISO_DATE));
    }


}