package com.nongxiange.unittesttutorial.java8.stream;

import cn.hutool.core.lang.TypeReference;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import org.junit.Test;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Stream;

public class StreamTest {
   
    @Test
    public void testIterate() {
        Stream.iterate(1, n -> n+1).limit(10).forEach(System.out::println);
    }
   // 随机生成10个Double元素的Stream并将其打印
    @Test
    public void testGenerate() {
        Stream.generate(() -> Math.random()).limit(10).forEach(System.out::println);
    }
    
   
    @Test
    public void testFilter() {
        Stream<String> s = Stream.of("test", "t1", "t2", "teeeee", "aaaa");
        //查找所有包含t的元素并进行打印
        s.filter(n -> n.contains("t")).forEach(System.out::println);
    }
    
    @Test
    public void testMap() {
        Stream<String> s = Stream.of("test", "t1", "t2", "teeeee", "aaaa");
        s.map(n -> n.concat(".txt")).forEach(System.out::println);
    }
    
    /**
     * 元素一对多转换：对原Stream中的所有元素使用传入的Function进行处理，每个元素经过处理后生成一个多个元素的Stream对象，
     * 然后将返回的所有Stream对象中的所有元素组合成一个统一的Stream并返回；
     */
    @Test
    public void testFlatMap() {
        Stream<String> s = Stream.of("test,terst2", "t1,t", "a,t2", "teeeee", "aaaa");
        s.flatMap(n -> Stream.of(n.split(","))).forEach(System.out::println);
    }
}
