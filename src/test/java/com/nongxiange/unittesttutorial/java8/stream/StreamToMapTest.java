package com.nongxiange.unittesttutorial.java8.stream;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *   集合中对象属性转map
 */
public class StreamToMapTest {
    List<Student> list = new ArrayList<>();
    @Before
    public void setUp() {
//        list.add(new Student("一年级二班", "小明",10));
//        list.add(new Student("一年级二班", "小芳", 23));
//        list.add(new Student("一年级二班", "小华",6));
//        list.add(new Student("一年级三班", "翠花",7));
//        list.add(new Student("一年级三班", "香兰",12));


        list.add(new Student(null, null, 23));
        list.add(new Student(null, null,6));
        list.add(new Student(null, null,7));
        list.add(new Student(null, null,12));
    }

    @Test(expected = IllegalStateException.class)
    public void toMap_exception_duplicateKey() {
        Map<String, String> map = list.stream().collect(Collectors.toMap(Student :: getClassName, Student :: getStudentName));
        System.out.println(map);
    }
    @Test
    public void toMap2() {
        List<Integer> a = list.stream().map(Student::getAge).collect(Collectors.toList());
        System.out.println(a);
    }
    /**
     * 重复时采用后面的value 覆盖前面的value
     */
    @Test
    public void toMap_key_override() {
        Map<String, String> map = list.stream().collect(Collectors.toMap(Student :: getClassName,
                Student :: getStudentName,(key1,key2)-> key2));
        System.out.println(map);
    }

    /**
     * 重复时将之前的value 和现在的value拼接或相加起来
     */
    @Test
    public void toMap_value_join() {
        Map<String, String> map = list.stream().collect(Collectors.toMap(Student :: getClassName,
                Student :: getStudentName,(key1,key2)-> key1 + "," + key2));
        System.out.println(map);
    }

    /**
     * 将重复key的数据变成一个集合
     */
    @Test
    public void toMap_duplicateKey2Collection() {
        Map<String, List<String>> map = list.stream().collect(Collectors.toMap(Student :: getClassName,
                 s -> {
                    List<String> studentNameList = new ArrayList<>();
                    studentNameList.add(s.getStudentName());
                    return studentNameList;
                },
                (List<String> value1,List<String> value2) -> {
                    value1.addAll(value2);
                    return value1;
                }));
        System.out.println(map);
    }
    
    @Test
    public void testMapToInt() {
        List<Integer> ages = list.stream().mapToInt(s -> Integer.valueOf(s.getAge()))
                // 一定要有 mapToObj，因为 mapToInt 返回的是 IntStream，因为已经确定是 int 类型了
                // 所有没有泛型的，而 Collectors.toList() 强制要求有泛型的流，所以需要使用 mapToObj
                // 方法返回有泛型的流
                .mapToObj(s->s)
                .collect(Collectors.toList());
    }
}
