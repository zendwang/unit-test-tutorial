package com.nongxiange.unittesttutorial.java8.stream;


public class Student {
    private String className;
    private String studentName;
    private int age;    
    public Student(String className, String studentName, int age) {
        this.className = className;
        this.studentName = studentName;
        this.age = age;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
    
    public int getAge() {
        return age;
    }
    
    public void setAge(final int age) {
        this.age = age;
    }
}
