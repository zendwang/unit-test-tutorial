package com.nongxiange.unittesttutorial.java8.stream;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Stream.flatMap，正如它的名称所猜测的，是map和一个flat行动。这意味着您首先对元素应用一个函数，然后将其扁平化。
 * Stream.map只对流应用函数，而不对流进行平坦处理。
 * https://zhaoshuxiang.blog.csdn.net/article/details/106083097
 */
public class Stream$FlatMap {
    @Test
    public void toFlatMap() {
        List<List<Integer>> outer = new ArrayList<>();
        List<Integer> inner1 = new ArrayList<>();
        inner1.add(1);
        List<Integer> inner2 = new ArrayList<>();
        inner2.add(2);  
        inner2.add(3);
        List<Integer> inner3 = new ArrayList<>();
        inner3.add(3);
        List<Integer> inner4 = new ArrayList<>();
        inner4.add(4);
        List<Integer> inner5 = new ArrayList<>();
        inner5.add(5);
        inner5.add(6);
        
        outer.add(inner1);
        outer.add(inner2);
        outer.add(inner3);
        outer.add(inner4);
        outer.add(inner5);
        List<Integer> result = outer.stream().flatMap(inner -> inner.stream().map(i -> i + 1)).distinct().collect(Collectors.toList());
        System.out.println(result);//[2, 3, 4, 5, 6, 7]
    }
}
