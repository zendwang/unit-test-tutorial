package com.nongxiange.unittesttutorial.java8;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static java.util.Calendar.DAY_OF_YEAR;
import static java.util.Calendar.YEAR;

/**
 * 非java8类
 */
public class CalendarTest {

    @Test
    public void test01() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.HOUR_OF_DAY, 1);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(sdf.format(cal.getTime()));
    }

    @Test
    public void test02() {
        Calendar cal = new Calendar.Builder().setCalendarType("japanese").setFields(YEAR, 1, DAY_OF_YEAR, 1).build();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(sdf.format(cal.getTime()));
    }
}
