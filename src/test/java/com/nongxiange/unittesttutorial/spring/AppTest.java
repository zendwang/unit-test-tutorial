package com.nongxiange.unittesttutorial.spring;

import com.nongxiange.unittesttutorial.spring.bean.IUserService;
import com.nongxiange.unittesttutorial.spring.bean.LifeCycleBean;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class AppTest {
    /**
     * 测试Spring Bean生命周期
     * */
    @Test
    public void testBeanLifeCycle() {
        ClassPathResource resource = new ClassPathResource("spring/spring-bean-life-cycle.xml");
        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
        factory.addBeanPostProcessor(new LifeCycleBean());
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(factory);
        reader.loadBeanDefinitions(resource);

        LifeCycleBean lifeCycleBean = (LifeCycleBean) factory.getBean("lifeCycleBean");
        lifeCycleBean.normalMethod();

        factory.destroySingleton("lifeCycleBean");
    }
    /**
     * BeanFactory与 FactoryBean 区别
     *
     * BeanFactory是Spring中IOC容器最核心的接口，
     * 遵循了IOC容器中所需的基本接口。例如我们很常见的：ApplicationContext，XmlBeanFactory 等等都使用了BeanFactory这个接口。
     *
     * FactoryBean是工厂类接口，当你只是想简单的去构造Bean，不希望实现原有大量的方法。
     * 它是一个Bean，不过这个Bean能够做为工厂去创建Bean，同时还能修饰对象的生成。
     *
     * FactoryBean比BeanFactory在生产Bean的时候灵活，还能修饰对象，带有工厂模式和装饰模式的意思在里面，
     * 不过它的存在还是以Bean的形式存在。
     *
     * https://www.cnblogs.com/dengpengbo/p/10493782.html#%E4%BA%8C%E3%80%81factorybean%E6%8E%A5%E5%8F%A3
     */
    @Test
    public void testFactoryBean() {
        BeanFactory bf = new XmlBeanFactory(new ClassPathResource("spring/spring-aware.xml"));
        Object bean =  bf.getBean(IUserService.class);
        System.out.println(bean);

        Object bean2 =  bf.getBean("&myFactoryBean");
        System.out.println(bean2);

    }

    /**
     * FactoryBean增强实现
     */
    @Test
    public void testFactoryBean2() {
        ApplicationContext ac = new ClassPathXmlApplicationContext("spring/spring-aware.xml");
        IUserService bean =   (IUserService) ac.getBean("myFactoryBeanPlus");
        System.out.println("****************");
        bean.doSome();
        System.out.println();
    }

}
