package com.nongxiange.unittesttutorial.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class LifeCycleBean implements BeanPostProcessor, BeanNameAware,
        BeanClassLoaderAware, BeanFactoryAware, InitializingBean, DisposableBean {

    private String name;

    public LifeCycleBean() {
        System.out.println("-----------------------1.Bean构造器");
    }
    public void setName(String name) {
        System.out.println("-----------------------2.Bean属性注入");
        this.name = name;
    }
    @Override
    public void setBeanName(String name) {
        System.out.println("-----------------------3.BeanNameAware的setBeanName()");
    }

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) {
        System.out.println("-----------------------4.BeanClassLoaderAware的setBeanClassLoader()");
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("-----------------------5.BeanFactoryAware的setBeanFactory()");
    }



    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("-----------------------6.[初始化之前]BeanPostProcessor的postProcessBeforeInitialization()");
        return bean;
    }
    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("-----------------------7.[初始化中，属性注入后]InitializingBean的afterPropertiesSet()");
    }
    public void initMethod() {
        System.out.println("-----------------------8.调用<bean>的init-method属性自定义的初始化方法");
    }
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("-----------------------9.[初始化之后]BeanPostProcessor的postProcessAfterInitialization()");
        return bean;
    }

    public void normalMethod() {
        System.out.println("-----------------------10.[容器初始化完成]程序运行，当JVM关闭时，销毁容器");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("-----------------------11.[容器销毁]DisposableBean的destroy()");
    }

    public void destroyMethod() {
        System.out.println("-----------------------12.[容器销毁]调用<bean>的destory-method属性自定义的销毁方法");
    }

    public String getName() {
        return name;
    }

}