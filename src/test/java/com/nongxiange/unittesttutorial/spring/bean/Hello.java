package com.nongxiange.unittesttutorial.spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class Hello implements BeanNameAware, BeanFactoryAware, ApplicationContextAware {

    private BeanFactory bf;
    private ApplicationContext context;

    @Override
    public void setBeanName(String name) {
        System.out.println("回调setBeanName方法  id属性是"+name);
    }
    public void init(){
        System.out.println("正在执行初始化方法init");
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("获得BeanFactory");
        this.bf = beanFactory;
    }

    public BeanFactory getBf() {
        return bf;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("获得ApplicationContext");
        this.context = applicationContext;
    }

    public ApplicationContext getContext() {
        return context;
    }
}