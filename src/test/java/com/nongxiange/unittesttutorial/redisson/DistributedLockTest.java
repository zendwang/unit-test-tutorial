package com.nongxiange.unittesttutorial.redisson;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
//import org.redisson.api.RLock;
//import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Slf4j
@RunWith(SpringRunner.class)
@TestPropertySource(value = "classpath:/redisson/application01.properties")
@SpringBootTest(classes = DistributedLockTest.class)
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@ComponentScan({"com.nongxiange.unittesttutorial.redisson"})
public class DistributedLockTest {
//
//    @Autowired
//    private RedissonClient redissonClient;
//
//    @Test
//    public void testSimple() {
//        RLock lock = redissonClient.getLock("xxxx");
//        try {
//            boolean bs = lock.tryLock(5,10, TimeUnit.SECONDS);
//            if (bs) {
//                // 业务代码
//                log.info("进入业务代码: ");
//
//                lock.unlock();
//            } else {
//                Thread.sleep(300);
//            }
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//            lock.unlock();
//        }
//    }
//
//    @Test
//    public void testMultiThread() throws InterruptedException {
//        final String key = "XXXXXXXXXXX2";
//        ExecutorService executor = Executors.newFixedThreadPool(5);
//
//        IntStream.range(0,2).forEach( i ->{
//            log.info("i={}", i);
//            executor.submit(new Runnable() {
//                @Override
//                public void run() {
//                    RLock lock = redissonClient.getLock(key);
//                    try {
//                        boolean bs = lock.tryLock(5,10, TimeUnit.SECONDS);
//                        if (bs) {
//                            // 业务代码
//                            log.info("currentThread:{} 进入业务代码",Thread.currentThread().getId());
//                            Thread.sleep(3000);
//                        } else {
//                            log.info("currentThread:{} 未进入业务代码",Thread.currentThread().getId());
//                        }
//                    } catch (InterruptedException e) {
//                        log.error("currentThread:{} ex:{}",Thread.currentThread().getId(), e);
//                    } finally {
//                        lock.unlock();
//                        log.info("currentThread:{} 释放锁", Thread.currentThread().getId());
//                    }
//                }
//            });
//
//
//        });
//
//        Thread.currentThread().join(15000);
//    }
}
