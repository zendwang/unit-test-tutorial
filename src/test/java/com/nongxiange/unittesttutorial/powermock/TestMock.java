package com.nongxiange.unittesttutorial.powermock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.*;//mock，when，times
import static org.junit.Assert.*;//assertTrue

@RunWith(PowerMockRunner.class)
@PrepareForTest(FTPClient.class)//在这声明才能powermock这个类的static成员
public class TestMock {
    //写测试用例最重要的是要把代码结构调整，
    //界面组件的代码与逻辑代码解耦（比如文本框的输入，不传文本框对象而直接获得文本框的输入文本String）

    @Test
    public void public_test(){
        FTPClient FTPclient= Mockito.mock(FTPClient.class);
        when(FTPclient.isConnected()).thenReturn(true);
        assertTrue(new FTPTest(FTPclient).isFTPConnected());
        verify(FTPclient,times(1)).isConnected();
    }

    @Test
    public void static_test(){
        PowerMockito.mockStatic(FTPClient.class);
        when(FTPClient.staticTest()).thenReturn("1234");
        assertEquals("1234", FTPClient.staticTest());
    }

    @Test
    public void static_test03(){
        ScheduledExecutorService e = Executors.newScheduledThreadPool(0);
        e.schedule(() -> {
            System.out.println("业务逻辑");
        }, 3, TimeUnit.SECONDS);
        e.shutdown();
    }
}