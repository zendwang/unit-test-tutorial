package com.nongxiange.unittesttutorial.powermock;

public class FTPTest {
    private FTPClient ftp;

    FTPTest(FTPClient ftp){
        this.ftp=ftp;
    }
    public boolean isFTPConnected() {
        if (!ftp.isConnected()) {
            return false;
        }
        return true;
    }
}
