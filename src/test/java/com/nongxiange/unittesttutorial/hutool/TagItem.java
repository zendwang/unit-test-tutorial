package com.nongxiange.unittesttutorial.hutool;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TagItem {
	private String tagName;
	private String tagValues;


	public void appendTagValue(String value) {
		this.tagValues = this.tagValues + ";" + value;
	}
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TagItem tagItem = (TagItem) o;

		return tagName != null ? tagName.equals(tagItem.tagName) : tagItem.tagName == null;
	}

	@Override
	public int hashCode() {
		return tagName != null ? tagName.hashCode() : 0;
	}
}
