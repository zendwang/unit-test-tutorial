package com.nongxiange.unittesttutorial.hutool;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileAppender;
import cn.hutool.core.lang.Console;
import cn.hutool.core.text.csv.CsvData;
import cn.hutool.core.text.csv.CsvReader;
import cn.hutool.core.text.csv.CsvRow;
import cn.hutool.core.text.csv.CsvUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.MD5;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;

import java.io.File;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExcelTest {

	@Test
	public void importUser() {

//		ExcelReader reader = ExcelUtil.getReader("D:\\同步盘\\明镜\\oneid.xls");
//		List<SysUserImportVo> all = reader.readAll(SysUserImportVo.class);
//		System.out.println(all.size());

//		String template = "INSERT INTO `sys_user` (`user_id`,`user_code`,`app_id`,`tenant_id`,`dept_id`,`user_name`,`nick_name`,`user_type`,`email`,`phonenumber`,`sex`,`avatar`,`password`,`status`,`del_flag`,`login_ip`,`login_date`,`create_by`,`create_time`,`update_by`,`update_time`,`remark`) VALUES " +
//				"({},'{}',108,8,{},'{}','{}','03','','{}','0','','$2a$10$qx3r4VvreIWXEhVHfgX0ReAsRSxBbx3./092w/3X9hrW.GWaGXGhy','0','0','116.62.74.72','2022-02-21 14:00:56','10800800','2022-02-21 16:00:00','10800800','2022-02-21 16:00:00','{\"quickbi\": \"{}\"}');";
//		for (SysUserImportVo vo: all) {
//			System.out.println(StrUtil.format(template, vo.getUserId(),vo.getUserCode(),vo.getDeptId(),vo.getUserName(),vo.getNickName(),vo.getPhonenumber(),vo.getRemark()));
//			break;
//		}

		CsvReader reader = CsvUtil.getReader();
		reader.setContainsHeader(true);
		//从文件中读取CSV数据
		CsvData data = reader.read(FileUtil.file("C:\\Users\\PC\\Desktop\\a\\20221122.csv"), CharsetUtil.CHARSET_GBK);
		List<CsvRow> rows = data.getRows();


		Map<String,String> map = new HashMap<>();
		//遍历行
		for (CsvRow csvRow : rows) {
			//getRawList返回一个List列表，列表的每一项为CSV中的一个单元格（既逗号分隔部分）
			String content = csvRow.get(0).substring(StrUtil.indexOf(csvRow.get(0), '=' ) + 1);
			content = content.replace("\"\"", "\"");
			JSONObject aa =  JSON.parseObject(content);
			String perfType = aa.getString("perfType");
			if (perfType.equals("1") || perfType.equals("2")) {
				map.putIfAbsent(aa.getString("perfNo"), content);
			}
		}
		Console.log("开始处理请求 size=" + map.size());
		//File file = new File("C:\\Users\\PC\\Desktop\\a\\a.txt");
		//FileAppender appender = new FileAppender(file, 16, true);
		for (Map.Entry<String,String> entry : map.entrySet()) {
			//appender.append(entry.getValue() + ";");
			Console.log("开始请求： data=" + entry.getValue());
			HttpRequest request = HttpRequest.post("https://api.idadt-tech.com/bdp/performance-stat").contentType("application/json");
			HttpResponse response = request.body(entry.getValue()).execute();
			Console.log("开始结束： response=" + JSON.toJSONString(response));
		}
		//appender.flush();
	}
}
