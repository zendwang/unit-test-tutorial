package com.nongxiange.unittesttutorial.hutool;

import cn.hutool.core.annotation.Alias;
import lombok.Data;

import java.io.Serializable;

@Data
public class TagDTO implements Serializable {

	private static final long serialVersionUID = -1L;

	@Alias("label_id")
	private String labelId;

	@Alias("label_name")
	private String labelName;

	@Alias("label_category")
	private String labelCategory;

	@Alias("label_category_id")
	private String labelCategoryOrder;

	@Alias("label_category_2")
	private String labelCategory2;

	@Alias("label_category_2_id")
	private String labelCategory2Order;

	@Alias("label_value")
	private String labelValue;
}
