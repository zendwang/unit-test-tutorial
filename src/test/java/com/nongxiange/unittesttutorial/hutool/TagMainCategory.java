package com.nongxiange.unittesttutorial.hutool;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TagMainCategory {
	private String mainCategory;
	private List<TagSubCategory> children = new ArrayList<>();
	private Integer displayOrder;
	public TagMainCategory() {
		this.children = new ArrayList<>();
		this.displayOrder = 0;
	}
}
