package com.nongxiange.unittesttutorial.hutool;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class StrUtilTest {

	@Test
	public void testLongSize() throws InterruptedException {
		System.out.println("---begin--");
		Thread.sleep(50000);
		Long[] aa = new Long[15000];
		int a = aa.length;
		System.out.println("---end--" + a);
	}

	@Test
	public void testIsAllEmpty() {
		List<Integer> list = new ArrayList<>();
		Assert.isTrue(ObjectUtil.isAllEmpty(null, null,"",list));
	}

	// 1、应用内无角色、当前参数无值： 跳过
	// 2、应用内无角色、当前参数有值：
	// 尝试应用内新增用户新增角色 发新增MQ 或值调整角色发更新
	// 3.应用内有角色，当前参数有值，相等：跳过
	@Test
	public void testRoleChange01() {
		List<Long> existRoleIds = new ArrayList<>();
		existRoleIds.add(27L);
		existRoleIds.add(28L);

		List<Long> currentRoleIds = new ArrayList<>();
		currentRoleIds.add(27L);
		currentRoleIds.add(28L);
		Collection<Long> results = CollUtil.disjunction(existRoleIds,currentRoleIds);
		Assert.isTrue(results.size() == 0);
	}

	@Test
	public void testRoleChange02() {
		List<Long> existRoleIds = new ArrayList<>();
		existRoleIds.add(27L);
		existRoleIds.add(28L);

		List<Long> currentRoleIds = new ArrayList<>();
		currentRoleIds.add(27L);
		List<Long> results = (ArrayList<Long>)CollUtil.disjunction(currentRoleIds,existRoleIds);
		Assert.isTrue(results.size() == 1 && results.get(0) == 28L);
	}


	@Test
	public void testRoleChange03() {
		List<Long> existRoleIds = new ArrayList<>();
		existRoleIds.add(27L);
		existRoleIds.add(28L);

		List<Long> currentRoleIds = new ArrayList<>();
		currentRoleIds.add(300L);
		Set<Long> results = CollUtil.unionDistinct(existRoleIds, currentRoleIds);
		Assert.notNull(results);
	}

}
