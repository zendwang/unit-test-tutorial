package com.nongxiange.unittesttutorial.hutool;

public class SysUserImportVo {

	private Long userId;

	private Long userCode;

	private Long deptId;

	private String userName;

	private String nickName;

	private String phonenumber;

	private String remark;

	public Long getUserId() {
		return userId;
	}

	public Long getUserCode() {
		return userCode;
	}

	public Long getDeptId() {
		return deptId;
	}

	public String getUserName() {
		return userName;
	}

	public String getNickName() {
		return nickName;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public String getRemark() {
		return remark;
	}
}
