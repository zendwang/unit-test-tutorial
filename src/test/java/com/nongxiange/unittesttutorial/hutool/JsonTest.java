package com.nongxiange.unittesttutorial.hutool;

import cn.hutool.core.map.MapUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.junit.Test;
import org.springframework.util.Assert;

import java.util.*;
import java.util.stream.Collectors;

public class JsonTest {


	@Test
	public void testJson() {
		String json = "{\"displayOrder\":5,\"children\":[{\"subCategory\":\"基本属性3\",\"displayOrder\":1,\"children\":[{\"tagName\":\"性别\",\"tagValues\":\"男\"}]},{\"subCategory\":\"基本属性\",\"displayOrder\":4,\"children\":[{\"tagName\":\"年龄\",\"tagValues\":\"30\"},{\"tagName\":\"贯籍\",\"tagValues\":\"ah;js\"},{\"tagName\":\"贯籍\",\"tagValues\":\"js\"}]}],\"mainCategory\":\"人口属性\"}";
		TagMainCategory response = JSON.parseObject(json, new TypeReference<TagMainCategory>(){});
		Assert.notNull(response);
	}
	@Test
	public void testToArray() {
		Person person = new Person();
		person.setName("xxxxx");
		person.setLoves(new ArrayList<>());
		System.out.println(JSONUtil.toJsonStr(person));
	}
	@Test
	public void testToBean() {
		String jsonText = "{\n" +
				"\"label_id\":\"RKJB0001\",\n" +
				"\"label_name\":\"性别\",  \n" +
				"\"label_category\":\"人口属性\",\n" +
				"\"label_category_2\":\"基本属性\",\n" +
				"\"label_value\":\"男\"\n" +
				"}";

		JSONObject jsonObject = JSONUtil.parseObj(jsonText);
		TagDTO item = JSONUtil.toBean(jsonObject, TagDTO.class);
		Assert.notNull(item);
	}

	@Test
	public void testToList() {
		String json = "[{\n" +
				"\"label_id\":\"RKJB0001\",\n" +
				"\"label_name\":\"性别\",  \n" +
				"\"label_category\":\"人口属性\",\n" +
				"\"label_category_2\":\"基本属性\",\n" +
				"\"label_value\":\"男\"\n" +
				"},\n" +
				"{\n" +
				"\"label_id\":\"RKJB0002\",\n" +
				"\"label_name\":\"年龄\",  \n" +
				"\"label_category\":\"人口属性\",\n" +
				"\"label_category_2\":\"基本属性\",\n" +
				"\"label_value\":\"30\"\n" +
				"},\n" +
				"{\n" +
				"\"label_id\":\"JYBD0003\",\n" +
				"\"label_name\":\"首次出单时间\",  \n" +
				"\"label_category\":\"交易属性\",\n" +
				"\"label_category_2\":\"保单属性\",\n" +
				"\"label_value\":\"2020-10\"\n" +
				"}]";
		JSONArray aa = JSONUtil.parseArray(json);
		List<TagDTO> items = JSONUtil.toList(aa, TagDTO.class);
		Assert.notNull(items);
	}

	@Test
	public void testToStreamGroup() {
		String json = "[{\n" +
				"\"label_id\":\"RKJB0001\",\n" +
				"\"label_name\":\"性别\",  \n" +
				"\"label_category\":\"人口属性\",\n" +
				"\"label_category_2\":\"基本属性3\",\n" +
				"\"label_category_id\": 5,\n" +
				"\"label_category_2_id\": 1,\n" +
				"\"label_value\":\"男\"\n" +
				"},\n" +
				"{\n" +
				"\"label_id\":\"RKJB0002\",\n" +
				"\"label_name\":\"年龄\",  \n" +
				"\"label_category\":\"人口属性\",\n" +
				"\"label_category_2\":\"基本属性\",\n" +
				"\"label_category_id\": 5,\n" +
				"\"label_category_2_id\": 4,\n" +
				"\"label_value\":\"30\"\n" +
				"},\n" +
				"{\n" +
				"\"label_id\":\"RKJB0005\",\n" +
				"\"label_name\":\"贯籍\",  \n" +
				"\"label_category\":\"人口属性\",\n" +
				"\"label_category_2\":\"基本属性\",\n" +
				"\"label_category_id\": 6,\n" +
				"\"label_category_2_id\": 4,\n" +
				"\"label_value\":\"ah\"\n" +
				"},\n" +
				"{\n" +
				"\"label_id\":\"RKJB0005\",\n" +
				"\"label_name\":\"贯籍\",  \n" +
				"\"label_category\":\"人口属性\",\n" +
				"\"label_category_2\":\"基本属性\",\n" +
				"\"label_category_id\": 6,\n" +
				"\"label_category_2_id\": 4,\n" +
				"\"label_value\":\"js\"\n" +
				"},\n" +
				"{\n" +
				"\"label_id\":\"JYBD0003\",\n" +
				"\"label_name\":\"首次出单时间\",  \n" +
				"\"label_category\":\"交易属性\",\n" +
				"\"label_category_2\":\"保单属性\",\n" +
				"\"label_category_id\": 6,\n" +
				"\"label_category_2_id\": 4,\n" +
				"\"label_value\":\"2020-10\"\n" +
				"}]";
		JSONArray jsonArray = JSONUtil.parseArray(json);
		List<TagDTO> items = JSONUtil.toList(jsonArray, TagDTO.class);

//		List<String> categoryList = items.stream().sorted((e1, e2) -> e1.getLabelCategory().compareTo(e2.getLabelCategory())).map(LabelItem::getLabelCategory).distinct().collect(Collectors.toList());
//		Assert.notNull(categoryList);

		Map<String,  Map<String, List<TagDTO>>> resultMap = items.stream()
				.collect(Collectors.groupingBy(s -> s.getLabelCategory(), Collectors.groupingBy(TagDTO::getLabelCategory2)));

		List<TagMainCategory> mainCategories =  new ArrayList<>();
		for (Map.Entry<String,  Map<String, List<TagDTO>>> entry: resultMap.entrySet()) {
			TagMainCategory mainCategory = new TagMainCategory();
			mainCategory.setMainCategory(entry.getKey());
			mainCategories.add(mainCategory);
			for (Map.Entry<String, List<TagDTO>> entry2 : entry.getValue().entrySet()) {
				boolean isFirst = true;
				TagItem tagItem = null;
				TagSubCategory subCategory = new TagSubCategory();
				subCategory.setSubCategory(entry2.getKey());
				mainCategory.getChildren().add(subCategory);
				for (TagDTO tagDto: entry2.getValue()) {
					if (isFirst) {
						mainCategory.setDisplayOrder(Integer.valueOf(tagDto.getLabelCategoryOrder()));
						subCategory.setDisplayOrder(Integer.valueOf(tagDto.getLabelCategory2Order()));
						isFirst = false;
					}
					tagItem = new TagItem(tagDto.getLabelName(),tagDto.getLabelValue());
					subCategory.addChild(tagItem);
				}
			}

			mainCategory.getChildren().sort(new Comparator<TagSubCategory>() {
				@Override
				public int compare(TagSubCategory o1, TagSubCategory o2) {
					return o1.getDisplayOrder().compareTo(o2.getDisplayOrder());
				}
			});
		}

		mainCategories.sort(new Comparator<TagMainCategory>() {
			@Override
			public int compare(TagMainCategory o1, TagMainCategory o2) {
				return o1.getDisplayOrder().compareTo(o2.getDisplayOrder());
			}
		});

		Assert.notNull(mainCategories, "not null");

//		System.out.println(JSONUtil.toJsonStr(mainCategories.get(0)));
	}
}
