package com.nongxiange.unittesttutorial.hutool;
import lombok.Data;

import java.util.List;


@Data
public class Person {

	private String name;

	private List<String> loves;
}
