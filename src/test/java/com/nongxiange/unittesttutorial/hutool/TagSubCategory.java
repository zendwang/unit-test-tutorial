package com.nongxiange.unittesttutorial.hutool;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TagSubCategory {
	private String subCategory;
	private List<TagItem> children;
	private Integer displayOrder;
	public TagSubCategory() {
		this.children = new ArrayList<>();
		this.displayOrder = 0;
	}

	public void addChild(TagItem child) {
		for (TagItem item: this.children) {
			if (item.equals(child)) {
				item.appendTagValue(child.getTagValues());
				break;
			}
		}
		this.children.add(child);
	}
}
