package com.nongxiange.unittesttutorial.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

/**
 * @ServerEndpoint 注解是一个类层次的注解，它的功能主要是将目前的类定义成一个websocket服务器端,
 * 注解的值将被用于监听用户连接的终端访问URL地址,客户端可以通过这个URL来连接到WebSocket服务器端
 */
@ServerEndpoint("/websocket")
@Component
@Slf4j
public class JXWebSocket {

	private AtomicLong count = new AtomicLong(0);

	/**
	 * 连接建立成功调用的方法
	 *
	 * @param session 可选的参数。session为与某个客户端的连接会话，需要通过它来给客户端发送数据
	 */
	@OnOpen
	public void onOpen(Session session) {
		log.info("有新连接加入！当前在线坐席人数为{}",count.incrementAndGet() );
	}

	/**
	 * 连接关闭调用的方法
	 */
	@OnClose
	public void onClose() {
		log.info("有连接关闭！当前在线坐席人数为{}" ,count.decrementAndGet() );
	}

	/**
	 * 收到客户端消息后调用的方法
	 *
	 * @param message 客户端发送过来的消息
	 * @param session 可选的参数
	 */
	@OnMessage
	public void onMessage(String message, Session session) {
		log.info("来自客户端的消息 {}", message);
		session.getAsyncRemote().sendText("backend:" + message);
	}

	/**
	 * 发生错误时调用
	 *
	 * @param session
	 * @param error
	 */
	@OnError
	public void onError(Session session, Throwable error) {
		log.error("发生错误: {}",error);
	}
}
