package com.nongxiange.unittesttutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
public class UnitTestTutorialApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnitTestTutorialApplication.class, args);
    }

}
