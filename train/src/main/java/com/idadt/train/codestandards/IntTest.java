package com.idadt.train.codestandards;

/**
 *
 * @author zendwang
 */
public class IntTest {
	public static void main(String[] args) {
		Integer a = 100, b = 100, c = 150, d = 150;
		System.out.println(a == b);// result:true
		System.out.println(c == d);// result:false
	}
}
